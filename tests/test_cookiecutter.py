# https://pytest-cookies.readthedocs.io/en/latest/getting_started/
import pytest
default_context = {
                    "email": "Your address email (eq. you@example.com)",
                    "repo_provider": "bitbucket.org",
                    "repo_username": "bb_un",
                    "project_name": "test_project",
                    "project_description": "A short description of the project",
                    "version": "0.1.0.dev",
                    "license": "MIT"
                }
#print(f"cookies: {cookies}")


def test_bake_returns_something(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result

def test_bake_isdir(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result

def test_bake_project_isdir(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result.project.isdir()

def test_bake_for_exceptions(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result.exception is None

def test_bake_for_successful_exit(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result.exit_code == 0

def test_bake_project_slug(cookies):
    result = cookies.bake(extra_context=default_context)
    assert result.project.basename == default_context['project_name']

#     #assert result.project.isdir()
