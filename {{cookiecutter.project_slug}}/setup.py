from setuptools import find_packages, setup
from pathlib import Path
mypackage_root_dir = Path(__file__).resolve().parent
with Path.open(mypackage_root_dir, 'VERSION') as version_file:
    version = version_file.read().strip()

setup(
    name='src',
    packages=find_packages(),
    version=version,
    description='{{ cookiecutter.project_description }}',
    author='{{ cookiecutter.email }}',
    license='{{ cookiecutter.license }}',
    package_data={
                "{{ cookiecutter.project_slug }}" : ['data/raw/*']
            },
)
