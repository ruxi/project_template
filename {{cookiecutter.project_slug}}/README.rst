**********************************
 {{ cookiecutter.project_slug }}
**********************************

{% if cookiecutter.repo_provider == 'bitbucket.org' %}


Build Status (master): TravisCI: |Build Status (master): TravisCI|
Build Status (dev):   TravisCI: |Build Status (dev):   TravisCI|
Build Status (master): Bitbucket Pipelines: |Build Status (master): Bitbucket Pipelines|
Build Status (dev): Bitbucket Pipelines: |Build Status (dev): Bitbucket Pipelines|
Bitbucket open issues: |Bitbucket open issues|
Bitbucket open pull requests: |Bitbucket open pull requests|
Code coverage (master): coveralls: |Code coverage (master): coveralls|
Code coverage (dev): coverall: |Code coverage (dev): coveralls|

.. |Build Status (master): TravisCI|            image:: https://travis-ci.com/{{cookiecutter.repo_project}}.svg?branch=master
   :target: https://travis-ci.com/{{cookiecutter.repo_project}}
.. |Build Status (dev): TravisCI| image:: https://travis-ci.com/{{cookiecutter.repo_project}}.svg?branch=dev
    :target: https://travis-ci.com/ruxi/project_template
    
.. |Build Status (master): Bitbucket Pipelines| image:: https://img.shields.io/bitbucket/pipelines/{{cookiecutter.repo_project}}/master/master.svg
.. |Build Status (dev): Bitbucket Pipelines|    image:: https://img.shields.io/bitbucket/pipelines/{{cookiecutter.repo_project}}/dev/master.svg
.. |Bitbucket open issues|                      image:: https://img.shields.io/bitbucket/issues/{{cookiecutter.repo_project}}/master.svg
.. |Bitbucket open pull requests|               image:: https://img.shields.io/bitbucket/pr/{{cookiecutter.repo_project}}/master.svg
.. |Code coverage (master): coveralls|          image:: https://img.shields.io/bitbucket/{{cookiecutter.repo_project}}/master/master.svg
.. |Code coverage (dev): coveralls|             image:: https://img.shields.io/bitbucket/{{cookiecutter.repo_project}}/dev/master.svg


{% elif cookiecutter.license == 'github.com' %}


+----------------------------------+------------------------------------+
| CI service                       | status                             |
+----------------------------------+------------------------------------+
| Build Status (master): TravisCI  | |Build Status (master): TravisCI|  |
+----------------------------------+------------------------------------+
| Build Status (dev):   TravisCI   | |Build Status (dev):   TravisCI|   |
+----------------------------------+------------------------------------+
| github open issues               | |github open issues|               |
+----------------------------------+------------------------------------+
| github open pull requests        | |github open pull requests|        |
+----------------------------------+------------------------------------+
| github forks                     | |github forks|                     |
+----------------------------------+------------------------------------+
| github stars                     | |github stars|                     |
+----------------------------------+------------------------------------+
| github watchers                  | |github watchers|                  |
+----------------------------------+------------------------------------+
| Code coverage (master)           | |Code coverage (master)|           |
+----------------------------------+------------------------------------+
| Code coverage (dev)              | |Code coverage (dev)|              |
+----------------------------------+------------------------------------+

.. |Build Status (master): TravisCI|  image:: https://img.shields.io/travis/{{cookiecutter.repo_project}}/master/master.svg
.. |Build Status (dev): TravisCI|     image:: https://img.shields.io/travis/{{cookiecutter.repo_project}}/dev/master.svg
.. |github open issues|               image:: https://img.shields.io/github/issues/{{cookiecutter.repo_project}}
.. |github open pull requests|        image:: https://img.shields.io/github/issues-pr/{{cookiecutter.repo_project}}
.. |github forks|                     image:: https://img.shields.io/github/forks/{{cookiecutter.repo_project}}?style=Fork
.. |github stars|                     image:: https://img.shields.io/github/stars/{{cookiecutter.repo_project}}?style=social
.. |github watchers|                  image:: https://img.shields.io/github/watchers/{{cookiecutter.repo_project}}?style=Watch
.. |Code coverage (master)|           image:: https://img.shields.io/github/{{cookiecutter.repo_project}}/master/master.svg
.. |Code coverage (dev)|              image:: https://img.shields.io/github/{{cookiecutter.repo_project}}/dev/master.svg

{% endif %}

{{ cookiecutter.project_description }}

######################################
Resources on how to use cookiecutter
######################################

This section can be deleted after initial set-up

***************************************
Repo Initialization: what do do first
***************************************



reproduce local analysis environment
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

edit ```environment.yml```

.. code-block:: bash

  conda env update
   conda activate {{cookiecutter.project_slug}}


-----------------------------------


install project as a python package
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. code-block::

   python setup.py develop

This enables calling user script in /src folder as modules

-----------------------------------


add ssh keys to version control cloud provider ( {{cookiecutter.repo_provider}})
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Allows passwordless authentication with version control provider. Only need to do this whenever you have a new device or create a new account (bitbucket or github)

Generate private/public keys


.. code-block:: bash

    cd ~/.ssh
    ssh-keygen -f <filename>
    ssh-add <filename>

add public key to cloud version control provider

:ref: `bitbucket_ssh`_

.. _bitbucket_ssh: https://help.github.com/en/github/authenticating-to-github/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent#generating-a-new-ssh-key

-----------------------------------

Working with large files
^^^^^^^^^^^^^^^^^^^^^^^^

By default, ```data/raw``` is being tracked by git large file storage (lfs) via .gitattributes config file. This may cause issues with authenticating via ssh, and should be removed if thats the case. git-lfs is not native to git, and needs to be installed seperately.

:ref: `git-lfs-tutorial`_

.. _git-lfs-tutorial: https://github.com/git-lfs/git-lfs/wiki/Tutorial


-----------------------------------

create version control repository with git
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Create new repository on {{cookiecutter.repo_provider}} via web-browser.



Add local project to git and push to cloud repo

.. code-block:: bash

   git init
   git add --all :/
   git commit -a -m 'first commit'
   git remote add origin git@{{cookiecutter.repo_provider}}:{{cookiecutter.repo_username}}/{{cookiecutter.project_slug}}.git # or use github.com
   git remote -v
   git push origin master

-----------------------------------

Modify README.rst
^^^^^^^^^^^^^^^^^^^^^

Edit this readme with project details. This file uses restructured test.

Here are some tutorials

:ref:

  `rst_cheatsheet`_
  `sphinx-docs`_


****************************************************************
Organizational best practices / suggestions
****************************************************************

Here are some notes on best practices on how to set-up and organize a data science repository

working with version control
----------------------------

create a development branch, and push to master with commit notes when ready

.. code-block:: bash

  # first initialization
  git checkout -b dev # crate dev branch

  # general workflow
  ## working on dev
  git checkout dev  # navigate to dev branch
  git status        # review changes
  git add --all :/
  git commit -a -m 'desc what changes were made'
  git push origin dev # push to dev branch

  ## ready to patch to master
  git checkout master
  git pull origin master # make sure local is up-to-date with cloud
  git checkout dev
  git rebase master      # merge dev and master branches
  ## git merge master    ## if conflicts need to be resolved
  git push origin master # save to cloud

  ## alternative way to replace master with dev code
  git checkout master            # go to master branch
  git reset --hard origin/dev    # replace master with local dev branch files
  git push origin master --force # force master changes to remote


References
^^^^^^^^^^^^^
- towardsdatascience: `general_git_workflow`_

.. _general_git_workflow: https://towardsdatascience.com/general-git-workflow-94cb22d1f18a


directory structure
-------------------

 - Files in `data/raw` are kept by as python package
 - Files in `data`...  `intermediate`,`processed`, `external`` are ignored



resources about best practices for repository structure
-------------------------------------------------------

:ref: `rst_cheatsheet`_
`scientific_cookiecutter`_

.. _scientific_cookiecutter_: https://nsls-ii.github.io/scientific-python-cookiecutter/philosophy.html

.. _rst_cheatsheet: https://thomas-cokelaer.info/tutorials/sphinx/rest_syntax.html

.. _sphinx-docs: https://www.sphinx-doc.org/en/1.5/markup/code.html
