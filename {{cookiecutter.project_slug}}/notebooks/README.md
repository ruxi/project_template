## notes for using jupyter notebooks in development mode

This project folder in meant to be installed as a python package.

```bash
python setup.py develop
```

When using jupyter notebook for development, its a good idea to have ```import``` modules autoreload on call

```python  
%load_ext autoreload
%autoreload 2
```

It's best to directly add user scripts to ```src``` folder as and import as a modules

```python
import src
```
