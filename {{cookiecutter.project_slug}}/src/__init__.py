from pathlib import Path as _Path
package_path    = _Path(__file__).resolve().parent.parent
module_path     = _Path(__file__).resolve().parent
data_path       = _Path(package_path, 'data')
notebooks_path  = _Path(package_path, 'notebooks')
reports_path    = _Path(package_path, 'reports')

# list folders in src path
list_modulepaths = [x for x in module_path.iterdir()
                if not (x.stem.startswith("__")) and (x.is_dir())
                ]
