#!/usr/bin/env python
__email__ = "lewis.r.liu@gmail.com"
__status__ = 'development'

import rpy2.robjects as robjects
from rpy2.robjects.packages import importr
import os.path
import tarfile


def pyGEOquery(GEO, OutputDirectory, cleanup=False, verbose=True):
    """
    downloads and extracts celfiles from NCBI
    when provided with GEO series ID

    args:
        GEO (str)

            must be a GEOseries ID for
            getGEOSuppFiles function

        OutputDirectory (str)

            path to where CEL files will be
            extracted to

        cleanup (boolean | default: False)

            whether to delete tmp folder
            used to store download microarray
            files

    requires:
        bioconda::bioconductor-biobase
        bioconda::bioconductor-geoquery

    TODO:
        add unit test
    """
    # load bioconductor libraries
    base = importr('base')
    pyGEOquery = importr('GEOquery')

    # temporary folder to hold downloaded files
    tmpdir = robjects.r("tempdir()")[0]

    # download if not previously downloaded
    dlpath = os.path.join(tmpdir,GEO)

    if (os.path.isdir(dlpath)) and (len(os.listdir(dlpath))>0):
        print("Dataset previously downloaded")
    else:
        print("downloading files...")
        pyGEOquery.getGEOSuppFiles(GEO=GEO, baseDir=tmpdir)

    if verbose:
        print("dlpath: \n\t{}\n".format(dlpath))

    # path handling
    tarball = os.path.join(
                            dlpath
                            , [x for x in os.listdir(dlpath) if os.path.splitext(x)[1] == '.tar'][0]
                          )
    if verbose:
        print("tarball: \n\t{}\n".format(tarball))

    # extracting
    tarfile.open(tarball, 'r').extractall(OutputDirectory)
    print("extractpath: \n\t{}\n".format(OutputDirectory))
