
# how to include data
## https://nsls-ii.github.io/scientific-python-cookiecutter/including-data-files.html
#!/usr/bin/env python'

from pathlib import Path as _Path
from .external_data import pyGEOquery
from .. import data_path

listdir  = [x for x in data_path.iterdir()
            if not x.resolve().stem.startswith("__")
            ]


#+------------------------
#| Add ducktyping for src.data.<foldername>
#|       based on class DataFolder
#+------------------------
class DataFolder(object):
    def __init__(self, path):
        self.path = path

    @property
    def dirname(self):
        return self.path.stem

    @property
    def listfiles(self):
        return [x for x in self.path.iterdir() if x.is_file()]

# add <foldername> to module scope.
## Except dictkeys: raw, intermediate, processed, external
def populate_datafolder_variables():
    return {path.name:DataFolder(path) for path in listdir}
__dict__(**populate_datafolder_variables()**)
