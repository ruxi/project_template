project_template
=====================

CI service | status
---|---
Build Status (master): TravisCI            | [![Build Status master travis](https://travis-ci.com/ruxi/project_template.svg?branch=master)](https://travis-ci.com/ruxi/project_template)
Build Status (dev): TravisCI               | [![Build Status dev travis](https://travis-ci.com/ruxi/project_template.svg?branch=dev)](https://travis-ci.com/ruxi/project_template)
Build Status (master): Bitbucket Pipelines | ![][Build Status (master): Bitbucket Pipelines]
Build Status (dev): Bitbucket Pipelines    | ![][Build Status (dev): Bitbucket Pipelines]
Bitbucket open issues                      | ![][Bitbucket open issues]
Bitbucket open pull requests               | ![][Bitbucket open pull requests]
Code coverage (master): coveralls          | ![][Code coverage (master): coveralls]
Code coverage (dev): coveralls             | ![][Code coverage (dev): coveralls]




[Build Status (master): Bitbucket Pipelines]: https://img.shields.io/bitbucket/pipelines/ruxi/project_templates/master/master.svg
[Build Status (dev): Bitbucket Pipelines]: https://img.shields.io/bitbucket/pipelines/ruxi/project_templates/dev/master.svg
[Bitbucket open issues]: https://img.shields.io/bitbucket/issues/ruxi/project_templates/master.svg
[Bitbucket open pull requests]: https://img.shields.io/bitbucket/pr/ruxi/project_templates/master.svg
[Code coverage (master): coveralls]: https://img.shields.io/coveralls/bitbucket/ruxi/project_templates/master/master.svg
[Code coverage (dev): coveralls]: https://img.shields.io/coveralls/bitbucket/ruxi/project_templates/dev/master.svg


cookie cutter for projects using conda

Requirements
------------

local install:
```bash
conda install -c conda-forge cookiecutter
```

conda env install:
```bash
conda env update
conda activate ruxi_project_template
```

Usage
-----
Generate a new Cookiecutter template layout: `cookiecutter bb:ruxi/project_template`    

use dev branch
```bash
cookiecutter git@bitbucket.org:ruxi/project_template.git --checkout dev
```


References
----------

 - [scientific python cookiecutter](https://nsls-ii.github.io/scientific-python-cookiecutter/)

License
-------
This project is licensed under the terms of the [MIT License](/LICENSE)
