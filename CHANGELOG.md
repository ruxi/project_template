# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

### 0.1.0dev
 - verified cookiecutter command works.
 - made fixes to cookiecutter.json and project files  
#### Added
 - package files
